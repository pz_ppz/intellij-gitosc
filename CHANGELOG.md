## [Unreleased]
- Optimized the storage of account login credentials.
- Add Web OAuth for Gitee login.

## [2022.3.3]
- Fix Bug - `AccessToken` always refresh when `git push`.

## [2022.3.2]
- Compatible with idea-2022.3
- Remove deprecated files
- Fix `Pull/Push` refresh `AccessToken` error.
- Upgrade `intellij-gradle-plugin`, `gradle-changelog-plugin` and `gradle`

## [2022.3.1]
- Compatible with idea-2022.3 release
- Remove some deprecated api
- Remove some deprecated files
- Upgrade `intellij-gradle-plugin`, `gradle-changelog-plugin` and `gradle`

## [2022.3.0]
- Compatible with idea-2022.3
- Remove some deprecated api
- Remove some deprecated files
- Upgrade `intellij-gradle-plugin` and `gradle`

## [2022.2.0]
- Compatible with idea-2022.1
- Remove some deprecated api 

## [2022.1.2]
- Optimized the storage of account login credentials.
- Add Web OAuth for Gitee login.
- Fix `Pull/Push` did not refresh AccessToken. 
- **`P.S.`** Because the certificate format has been modified, please reset your accounts.

## [2022.1.1]
- Optimized the storage of account login credentials.
- Add Web OAuth for Gitee login.
- **`P.S.`** Because the certificate format has been modified, please reset your accounts.

## [2022.1.0]
- Compatible with idea-2022.1
- Remove some deprecated api
- Fix task issue info request error